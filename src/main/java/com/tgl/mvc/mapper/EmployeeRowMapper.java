package com.tgl.mvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.tgl.mvc.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {
  public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
    Employee emp = new Employee();
    emp.setId(rs.getLong("id"));
    emp.setChName(rs.getString("ch_name"));
    emp.setEngName(rs.getString("eng_name"));
    emp.setHeight(rs.getDouble("height"));
    emp.setWeight(rs.getDouble("weight"));
    emp.setBmi(rs.getDouble("bmi"));
    emp.setEmail(rs.getString("email"));
    emp.setPhone(rs.getString("phone"));
    return emp;
  }
}
