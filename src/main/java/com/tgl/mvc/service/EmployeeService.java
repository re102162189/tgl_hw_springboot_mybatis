package com.tgl.mvc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgl.mvc.dao.EmployeeMyBatisDao;
import com.tgl.mvc.exception.EmployeeNotValidException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;
import com.tgl.mvc.util.EmployeeValidation;

/**
 * 
 * @author kite.chen Designed for business logic
 *
 */
@Service
public class EmployeeService {

  @Autowired
  private EmployeeMyBatisDao employeeMyBatisDao;

  public long insert(Employee employee) {
    employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
    return employeeMyBatisDao.insert(employee);
  }

  public boolean delete(long employeeId) {
    return employeeMyBatisDao.delete(employeeId) > 0;
  }

  public boolean update(Employee employee) {
    employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
    return employeeMyBatisDao.update(employee) > 0;
  }

  public Employee findById(long employeeId) {
    Employee result = employeeMyBatisDao.findById(employeeId);
    if (result == null) {
      return null;
    }
    String chName = result.getChName();
    String maskedName = DataUtil.maskChName(chName);
    result.setChName(maskedName);
    return result;
  }

  public boolean batchUpdateRaw(List<String> rawList) throws EmployeeNotValidException {
    List<Employee> empList = new ArrayList<>();
    for (String raw : rawList) {
      String[] data = raw.split(",");
      if (data == null || data.length != 6) {
        continue;
      }
      
      
      Employee employee = new Employee();
      employee.setChName(data[0]);
      employee.setEngName(data[1]);
      employee.setEmail(data[2]);
      employee.setPhone(data[3]); 
      try {        
        employee.setWeight(Double.parseDouble(data[4]));
        employee.setHeight(Double.parseDouble(data[5]));
      } catch (NumberFormatException e) {
        throw new EmployeeNotValidException(String.format("raw data is not valid: %s", raw));
      }
      employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));

      if (!EmployeeValidation.isValid(employee)) {
        throw new EmployeeNotValidException(String.format("raw data is not valid: %s", raw));
      }

      empList.add(employee);
    }
    return employeeMyBatisDao.batchUpdate(empList);
  }
}
