package com.tgl.mvc.util;

public final class DataUtil {

  private DataUtil() {}

  public static String maskChName(String chName) {
    if (chName == null || chName.length() == 0) {
      return "";
    }

    StringBuilder mask = new StringBuilder();
    mask.append(chName.substring(0, 1));
    mask.append("*");
    if (chName.length() == 3) {
      mask.append(chName.substring(2, 3));
    }
    if (chName.length() == 4) {
      mask.append("*");
      mask.append(chName.substring(3, 4));
    }
    return mask.toString();
  }

  public static double bmi(double height, double weight) {
    return weight / Math.pow((height / 100), 2);
  }

}
