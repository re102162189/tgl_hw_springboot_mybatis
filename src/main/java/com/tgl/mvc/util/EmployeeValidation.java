package com.tgl.mvc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.tgl.mvc.model.Employee;

public final class EmployeeValidation {

  /**
   * email validation permitted by RFC 5322
   */
  private static final Pattern EMAIL_PATTERN =
      Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

  private static final Pattern ENG_NAME_PATTERN = Pattern.compile("^[a-zA-Z]+-[a-zA-Z]+$");

  private static final Pattern PHONE_PATTERN = Pattern.compile("^[0-9]{4}$");

  private EmployeeValidation() {}

  public static boolean isValid(Employee employee) {
    // check email
    Matcher matcher = EMAIL_PATTERN.matcher(employee.getEmail());
    boolean isValid = matcher.matches();
    if (!isValid) {
      return false;
    }

    // check English name
    matcher = ENG_NAME_PATTERN.matcher(employee.getEngName());
    isValid = matcher.matches();
    if (!isValid) {
      return false;
    }

    // check Chinese name
    String chName = employee.getChName();
    for (int i = 0; i < chName.length();) {
      int codepoint = chName.codePointAt(i);
      i += Character.charCount(codepoint);
      if (Character.UnicodeScript.of(codepoint) != Character.UnicodeScript.HAN) {
        return false;
      }
    }

    matcher = PHONE_PATTERN.matcher(employee.getPhone());
    isValid = matcher.matches();
    if (!isValid) {
      return false;
    }

    return true;
  }
}
